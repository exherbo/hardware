# Copyright 2019-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

CUDA_VERSION=11

SUMMARY="GPU-accelerated library of primitives for deep neural networks"
DESCRIPTION="
The NVIDIA CUDA Deep Neural Network library (cuDNN) is a GPU-accelerated library of primitives for
deep neural networks. cuDNN provides highly tuned implementations for standard routines such as
forward and backward convolution, pooling, normalization, and activation layers.

Deep learning researchers and framework developers worldwide rely on cuDNN for high-performance GPU
acceleration. It allows them to focus on training neural networks and developing software
applications rather than spending time on low-level GPU performance tuning. cuDNN accelerates
widely used deep learning frameworks, including Caffe, Caffe2, Chainer, Keras, MATLAB, MxNet,
TensorFlow, and PyTorch. For access to NVIDIA optimized deep learning framework containers, that
has cuDNN integrated into the frameworks, visit NVIDIA GPU CLOUD to learn more and get started.
"
HOMEPAGE="https://developer.nvidia.com/${PN}"
DOWNLOADS="
    manual: ${PN}-linux-x86_64-${PV}_cuda${CUDA_VERSION}-archive.tar.xz
"

LICENCES="NVIDIA-cuDNN"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="fetch strip test"

DEPENDENCIES="
    run:
        dev-util/nvidia-cuda-toolkit[>=${CUDA_VERSION}]
"

WORK=${WORKBASE}/${PN}-linux-x86_64-${PV}_cuda${CUDA_VERSION}-archive

pkg_setup() {
    exdirectory --allow /opt
}

src_install() {
	insinto /opt/cuda/targets/x86_64-linux
	doins -r {include,lib}
}

