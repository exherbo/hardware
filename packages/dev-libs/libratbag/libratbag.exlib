# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=libratbag tag=v${PV} ] python [ blacklist=2 multibuild=false ]
require meson
require flag-o-matic
require systemd-service udev-rules

export_exlib_phases src_prepare

SUMMARY="A library to configure gaming mice"

LICENCES="MIT"
SLOT="0"

MYOPTIONS="
    doc [[ description = [ Build API documentation ] ]]
    ( providers: elogind systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        core/json-glib
        dev-lang/python:*[>=3.8]
        dev-lang/swig
        dev-libs/glib:2
        dev-libs/libunistring:=
        x11-libs/libevdev
        providers:elogind? ( sys-auth/elogind[>=227] )
        providers:systemd? ( sys-apps/systemd[>=227] )
    test:
        app-admin/chrpath
        dev-libs/check[>=0.9.10]
        dev-python/lxml[python_abis:*(-)?]
        dev-util/valgrind
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dudev-dir=${UDEVDIR}
    -Dsystemd-unit-dir=${SYSTEMDSYSTEMUNITDIR}
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc documentation'
    'providers:systemd logind-provider systemd elogind'
    'providers:systemd systemd'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

libratbag_src_prepare() {
    meson_src_prepare

    # Replace generic python3 shebang with python$(python_get_abi)
    edo find ./ -name "*.py" -exec sed "s/python3/python$(python_get_abi)/" -i {} \;
    edo find ./tools -name "*.in" -exec sed "s/python3/python$(python_get_abi)/" -i {} \;
}

