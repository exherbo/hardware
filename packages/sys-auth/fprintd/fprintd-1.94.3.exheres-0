# Copyright 2014 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="v${PV}"
WORK="${WORKBASE}/${PN}-${MY_PV}"

require gitlab [ prefix=https://gitlab.freedesktop.org user=libfprint new_download_scheme=true ]
require meson pam systemd-service test-dbus-daemon

SUMMARY="D-Bus service to access fingerprint readers"
HOMEPAGE="https://fprint.freedesktop.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    doc
    gtk-doc
    pam
    systemd [[ description = [ Install system service files ] ]]
    ( providers: elogind systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        doc? (
            dev-lang/perl:= [[ note = [ for pod2man ] ]]
        )
        gtk-doc? (
            dev-doc/gtk-doc
            dev-libs/libxslt [[ note = [ for xsltproc ] ]]
        )
    build+run:
        base/libfprint[>=1.94.0]
        dev-libs/dbus-glib:1
        dev-libs/glib:2[>=2.56]
        sys-auth/polkit:1[>=0.91]
        pam? ( 
            sys-libs/pam
            sys-libs/pam_wrapper
        )
        systemd? ( sys-apps/systemd )
        providers:elogind? ( sys-auth/elogind )
        providers:systemd? ( sys-apps/systemd )
    test:
        dev-libs/libxml2:2.0 [[ note = [ for xmllint ] ]]
        dev-python/dbus-python
        dev-python/python-dbusmock
        dev-python/pycairo
        gnome-bindings/pygobject:3
"

MESON_SRC_CONFIGURE_OPTIONS=(
    'pam -Dpam_modules_dir='$(getpam_mod_dir)
    'systemd -Dsystemd_system_unit_dir='${SYSTEMDSYSTEMUNITDIR}
    'providers:elogind -Dlibsystemd=libelogind'
    'providers:systemd -Dlibsystemd=libsystemd'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    pam
    'doc man'
    'gtk-doc gtk_doc'
    systemd
)

# src_test completes successfully but there aree a bunch of leftover dbus processes:
# dbus-daemon --fork --print-address=1 --print-pid=1 --config-file=/var/tmp/paludis/build/sys-auth-fprintd-1.94.2/temp/dbusmock_data_5e_1vcay/dbusmock_system_cfg
# which causes cave to hang
RESTRICT='test'
src_test() {
    esandbox allow_net --connect "unix-abstract:/tmp/.X11-unix/X1"
    esandbox allow_net --connect "unix:/run/systemd/userdb/io.systemd.DynamicUser"
    esandbox allow_net --bind "unix-abstract:*/bus/python3/system"
    test-dbus-daemon_run-tests meson_src_test
    esandbox disallow_net --bind "unix-abstract:*/bus/python3/system"
    esandbox disallow_net --connect "unix:/run/systemd/userdb/io.systemd.DynamicUser"
    esandbox disallow_net --connect "unix-abstract:/tmp/.X11-unix/X1"
}

pkg_postinst() {
    elog "To enable fingerprint login add:"
    elog "    auth      sufficient      pam_fprintd.so"
    elog "as the _first_ line to /etc/pam.d/system-auth"
    elog "But remember to run 'fprintd-enroll <username>' before"
}

